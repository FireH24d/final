package com.example.demo.Controller;

import com.example.demo.Entity.Login;
import com.example.demo.Service.LoginService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LoginController {
    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(loginService.getAll());
    }

    @RequestMapping(value = "/login/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id) {
        return ResponseEntity.ok(loginService.getById(id));
    }

    @RequestMapping(value = "/login/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id) {
        loginService.delete(id);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Login login) {
        return ResponseEntity.ok(loginService.update(login));
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> signin(@RequestBody Login login) {
        try {
            return ResponseEntity.ok(loginService.signIn(login));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST, headers = "Accept=application/json")
    public void register(@RequestBody Login login)  {
             loginService.register(login);
        }



}
