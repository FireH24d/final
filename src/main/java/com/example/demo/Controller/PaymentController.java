package com.example.demo.Controller;

import com.example.demo.Entity.Login;
import com.example.demo.Entity.Payment;
import com.example.demo.Service.LoginService;
import com.example.demo.Service.PaymentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PaymentController {
    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @RequestMapping(value="/payment",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(paymentService.getAll());
    }
    @RequestMapping(value="/payment/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(paymentService.getById(id));}
    @RequestMapping(value = "/payment/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        paymentService.delete(id);
    }
    @RequestMapping(value="/payment",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Payment payment){
        return ResponseEntity.ok(paymentService.update(payment));
    }
}
