package com.example.demo.Repository;

import com.example.demo.Entity.Login;
import com.example.demo.Entity.Order;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface LoginRepository extends CrudRepository<Login, Long> {
    Login findByLoginAndPassword(String log,String password);
 Login findByToken(String token);
    @Transactional
    @Modifying
    @Query(
            value = "insert into login (shop_id, login, password,token) values ( :shop_id, :login,:password,:token)",
            nativeQuery = true)
    void insertAccount(  long shop_id,@Param("login") String login,
                       @Param("password") String password,String token);

}
