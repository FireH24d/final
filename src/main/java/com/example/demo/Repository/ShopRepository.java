package com.example.demo.Repository;


import com.example.demo.Entity.Shop;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ShopRepository extends CrudRepository<Shop, Long> {
    @Query(
            value = "SELECT id FROM shop ORDER BY id DESC LIMIT 1",
            nativeQuery = true)
    long getLatest();
    Shop findById(long customerId);

    @Transactional
    @Modifying
    @Query(
            value = "insert into shop ( name, address, phone) values ( :name, :address, :phone)",
            nativeQuery = true)
    void insert( @Param("name") String name ,
                @Param("address") String address, @Param("phone") String contactNumber);


}


