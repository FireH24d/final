package com.example.demo.Service;

import com.example.demo.Entity.Login;
import com.example.demo.Entity.Shop;
import com.example.demo.Repository.LoginRepository;
import com.example.demo.Repository.ShopRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class ShopService {
    private final ShopRepository shopRepository;
    private final LoginRepository loginRepository;

    public ShopService(ShopRepository shopRepository, LoginRepository loginRepository) {
        this.shopRepository = shopRepository;
        this.loginRepository = loginRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(shopRepository.findAll());
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(shopRepository.findById(id));
    }

    public void delete(long id){
        shopRepository.deleteById(id);
    }
    public Shop update(@RequestBody Shop shop){
        return  shopRepository.save(shop);
    }
    public Shop getCustomerByToken(String token) {
        Login login = loginRepository.findByToken(token);
        return shopRepository.findById(login.getShop_id());

    }
    public void register( Shop shop) {
        String organizationName = shop.getName();
        String address = shop.getAddress();
        String contactNumber = shop.getPhone();
        shopRepository.insert( organizationName, address, contactNumber);

    }


}
