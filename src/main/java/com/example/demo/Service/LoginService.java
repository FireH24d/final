package com.example.demo.Service;

import com.example.demo.Entity.Login;
import com.example.demo.Repository.LoginRepository;
import com.example.demo.Repository.ShopRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LoginService {
    private final LoginRepository loginRepository;
    private final ShopRepository shopRepository;

    public LoginService(LoginRepository loginRepository, ShopRepository shopRepository) {
        this.loginRepository = loginRepository;
        this.shopRepository = shopRepository;
    }

    public List<Login> getAll(){
        return (List<Login>)loginRepository.findAll();
    }
    public Optional<Login> getById(long id) {
        return loginRepository.findById(id);
    }

    public void delete(long id){
        loginRepository.deleteById(id);
    }
    public Login update(@RequestBody Login login){
        return  loginRepository.save(login);
    }
    @Transactional
    public Login signIn(Login login) throws Exception {
      String log=login.getLogin();
      String password=login.getPassword();
      Login auth= loginRepository.findByLoginAndPassword(log,password);

      if(auth==null){
throw new Exception("Not logined");
      }
        else{
            String token= UUID.randomUUID().toString();
            auth.setToken(token);
             loginRepository.save(auth);
return auth;
        }
    }

    public void register(Login login) {
        String log=login.getLogin();
        String password=login.getPassword();
        long shop_id=shopRepository.getLatest();
        loginRepository.insertAccount(shop_id, log, password, "1");
    }

}
