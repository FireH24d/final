package com.example.demo.Service;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.OrderItem;
import com.example.demo.Entity.Product;
import com.example.demo.Entity.Status;
import com.example.demo.OrderReturn;
import com.example.demo.Repository.OrderItemRepository;
import com.example.demo.Repository.OrderRepository;
import com.example.demo.Repository.ProductRepository;
import com.example.demo.Repository.StatusRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderItemService {
    private final OrderItemRepository orderItemRepository;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final StatusRepository statusRepository;


    public OrderItemService(OrderItemRepository orderItemRepository, OrderRepository orderRepository, ProductRepository productRepository, StatusRepository statusRepository) {
        this.orderItemRepository = orderItemRepository;
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.statusRepository = statusRepository;
    }

    public List<OrderItem> getAll() {
        return (List<OrderItem>) orderItemRepository.findAll();
    }

    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(orderItemRepository.findById(id));
    }

    public void delete(long id) {
        orderItemRepository.deleteById(id);
    }

    public OrderItem update(@RequestBody OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }

    public void save(long shop_id, ArrayList<Integer> product_id, ArrayList<Integer> quantity, ArrayList<Float> price) {
        float total_price=0;
        for (int id : product_id) {
            int q = quantity.get(id);
            float p = price.get(id);
            total_price+=q*p;
            Order order = orderRepository.findByShopId(shop_id);
            orderItemRepository.insertorderitem(order.getId(), id, q, p);
            orderRepository.updateprice(total_price,order.getId());

        }

    }
@Transactional
   public List<Order> getByShopID(long shop_id) {
        List<Order> list = orderRepository.findordersbyshop_id(shop_id);
    for(Order order:list){
           order.setOrderItems(orderItemRepository.getByorder_id(order.getId()));
       }
    return list;
    }



    @Transactional
    public List<OrderReturn> getDashboard(){
        List<OrderReturn> or=new ArrayList<>();
        List<Product> prod = new ArrayList<>();
        Long status= statusRepository.findByIdMax();
    List<OrderItem> orderItems=  orderItemRepository.getBySmth(status);
            float total_price = 0;
            int quantity = 0;
            for (OrderItem orderitems : orderItems) {
                prod.add(productRepository.findById(orderitems.getProduct_id()));
            }
            for (Product product : prod) {

                quantity = orderItemRepository.getBySumProduct(product.getId());
                total_price = product.getPrice() * quantity;
                OrderReturn orderReturn = new OrderReturn(product.getId(),product.getName(), quantity, total_price);
                if (!or.contains(orderReturn)) {
                    or.add(orderReturn);
            }}

        return or;
    }




}
