package com.example.demo.Service;

import com.example.demo.Entity.Handlings;
import com.example.demo.Repository.HandlingsRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class HandlingsService {
    private final HandlingsRepository handlingsRepository;

    public HandlingsService(HandlingsRepository handlingsRepository) {
        this.handlingsRepository = handlingsRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(handlingsRepository.findAll());
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(handlingsRepository.findById(id));
    }
    public void delete(long id){
        handlingsRepository.deleteById(id);
    }
    public Handlings update(@RequestBody Handlings person){
        return  handlingsRepository.save(person);
    }

}
