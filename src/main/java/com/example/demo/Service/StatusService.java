package com.example.demo.Service;

import com.example.demo.Entity.Status;
import com.example.demo.Repository.StatusRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class StatusService {
    private final StatusRepository statusRepository;

    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    public List<Status> getAll(){
        return (List<Status>) statusRepository.findAll();
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(statusRepository.findById(id));
    }

    public void delete(long id){
        statusRepository.deleteById(id);
    }
    public Status update(@RequestBody Status shop){
        return  statusRepository.save(shop);
    }
    public List<String> getNames(){
        return statusRepository.getNames();
    }
}
